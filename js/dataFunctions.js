export const getSearchTerms = () => {
  const rawSearchTerms = document.getElementById('search').value.trim()
  const regex = /[ ]{2,}/gi
  // replace more than one space with one
  const searchTerms = rawSearchTerms.replaceAll(regex, ' ')
  return searchTerms
}

export const retrieveSearchResults = async searchTerms => {
  const wikiSearchString = getWikiSearchString(searchTerms)
  const wikiSearchResults = await requestData(wikiSearchString)
  let resultArray = []
  if (wikiSearchResults.hasOwnProperty('query')) {
    resultArray = processWikiresults(wikiSearchResults.query.pages)
  }
  return resultArray
}

const getWikiSearchString = searchTerms => {
  const maxChars = getMaxChars()
  const rawSearchString = `https://en.wikipedia.org/w/api.php?action=query&generator=search&gsrsearch=${searchTerms}&gsrlimit=20&exchars=${maxChars}&format=json&prop=pageimages|extracts&exintro&explaintext&exlimit=max&formatversion=2&origin=*`
  const searchString = encodeURI(rawSearchString)
  return searchString
}

const getMaxChars = () => {
  const width = window.innerWidth || document.body.clientWidth
  let maxChars
  if (width < 414) {
    maxChars = 65
  }
  if (width >= 414 && width < 1400) {
    maxChars = 100
  }
  if (width >= 1400) {
    maxChars = 130
  }
  return maxChars
}

const requestData = async searchString => {
  try {
    const response = await fetch(searchString)
    const data = await response.json()
    return data
  } catch (error) {
    console.error(error)
  }
}

const processWikiresults = results => {
  const resultArray = []
  Object.keys(results).forEach(key => {
    const id = key
    const title = results[key].title
    const text = results[key].extract
    const image = results[key].hasOwnProperty('thumbnail') ? results[key].thumbnail.source : null
    const item = {
      id,
      title,
      image,
      text,
    }
    resultArray.push(item)
  })
  return resultArray
}
