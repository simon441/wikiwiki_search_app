import { getSearchTerms, retrieveSearchResults } from './dataFunctions.js'
import { clearPushListener, clearSearchText, setSearchFocus, showClearTextButton } from './searchBar.js'
import { buildSearchResults, clearStatsLine, deleteSearchResults, setStatsLine } from './searchResults.js'

document.addEventListener('readystatechange', e => {
  if (e.target.readyState === 'complete') {
    initApp()
  }
})

const initApp = () => {
  setSearchFocus()

  // 3 listeners clear text
  const search = document.getElementById('search')
  search.addEventListener('input', showClearTextButton)

  const clear = document.getElementById('clear')
  clear.addEventListener('click', clearSearchText)
  clear.addEventListener('keydown', clearPushListener)

  const form = document.getElementById('searchBar')
  form.addEventListener('submit', submitTheSearch)
}

// Procedural "workflow" functions
const submitTheSearch = e => {
  e.preventDefault()
  deleteSearchResults()

  // process the search
  processTheSearch()

  // set the focus on the input
  setSearchFocus()
}

// Procedural
const processTheSearch = async () => {
  //  clear the stats line
  clearStatsLine()
  const searchTerms = getSearchTerms()
  // if search is empty return
  if (searchTerms === '') {
    return
  }
  const resultArray = await retrieveSearchResults(searchTerms)
  if (resultArray.length) {
    buildSearchResults(resultArray)
  }
  setStatsLine(resultArray.length)
}
